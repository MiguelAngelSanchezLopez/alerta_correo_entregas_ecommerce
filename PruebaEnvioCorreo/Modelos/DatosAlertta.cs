﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaEnvioCorreo.Modelos
{
    public class DatosAlertta
    {
        public string FechaSubidaViaje;
        public string NroEntrega;
        public string Nombre;
        public string Entregado;
        public string FechaEntrega;
        public string MotivoRechazo;
        public string Placa;
        public string Cant_dias;
        public string LatProgramado;
        public string LonProgramado;
        public string LatEntrega;
        public string LonEntrega;
        public string MotivoSeleccionado;
        public string TotalViajes;
        public string NombreProducto;
        public string Linea;
        public string NombreCliente;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaEnvioCorreo.Modelos
{
    public class CorreoEnviado
    {
        public int id;
        public int mob_nro_ent;
        public string correo_al_que_se_envio="";
        public DateTime fecha_en_la_que_se_envio;
    }
}

﻿using CapaDatos.Controladores;
using CapaDatos.Modelos;
using CrossCutting;
using PruebaEnvioCorreo.Controladores;
using PruebaEnvioCorreo.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.IO;

namespace PruebaEnvioCorreo
{
    class Program
    {
        public static int SEGUNDOS_ESPERA = 0;

        public static string ObtenerAsunto()
        {
            return ConfigurationManager.AppSettings["ASUNTO_CORREO"].ToString();
        }

        public static string ObtenerMailsDestinoPorNombreAlerta() {
            string nombreAlerta= ConfigurationManager.AppSettings["NOMBRE_ALERTA"].ToString();

            try
            {
                string destinatarios = "";
                ControladorGeneral controladorGeneral = new ControladorGeneral();

                controladorGeneral.InicializarConexion();
                
                controladorGeneral.conexionSQL.Open();

                controladorGeneral.EstablecerProcedimientoAlmacenado("OBTENER_DESTINATARIOS_POR_NOMBRE_ALERTA");

                controladorGeneral.EstablecerParametroProcedimientoAlmacenado(nombreAlerta, "string", "@NombreAlerta");

                destinatarios=controladorGeneral.EjecutarProcedimientoAlmacenadoEscalar();

                controladorGeneral.conexionSQL.Close();

                return destinatarios;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public static string ObtenerMailsDestino()
        {
            //obtener destinatarios consultando en la bd

            return ObtenerMailsDestinoPorNombreAlerta();

            //return ConfigurationManager.AppSettings["MAILS_DESTINO"].ToString();
        }

        public static string ObtenerCuerpoCorreo()
        {
            /*
            string ruta = System.AppContext.BaseDirectory + "..\\..\\";

            String ArchivoCuerpoCorreo = ConfigurationManager.AppSettings["CUERPO_CORREO"].ToString();

            string cuerpoCorreo = File.ReadAllText(ruta + ArchivoCuerpoCorreo, Encoding.UTF8);

            return cuerpoCorreo;
            */
            return "";
        }

        public static void EnviarCorreo(string destinatario, string asunto, string bodyConDatos)
        {
            Mail mail = new Mail();
            mail.SendMail(destinatario, asunto, bodyConDatos);
        }

        public static void EnviarCorreoYGuardarRegistro(string destinatario, string asunto, string body, EntregaMovilPrueba registro, ControladorCorreoEnviado controladorCorreoEnviado, CorreoEnviado correoEnviadoTemporal)
        {
            try
            {
                Console.WriteLine("enviando...");

                string bodyConDatos;
                bodyConDatos = body + "<span style=\"font-size:12pt; font-weight:bold\">" +
                "mob_nro_ent:" + registro.mob_nro_ent + "&nbsp;&nbsp;" +
                "mob_cab_nro_int:" + registro.mob_cab_nro_int + "&nbsp;&nbsp;" +
                "mob_cab_id_usr:" + registro.mob_cab_id_usr + "&nbsp;&nbsp;" +
                "</span><br>&nbsp;";

                EnviarCorreo(destinatario, asunto, bodyConDatos);

                //guardar listado de envio de correo en tabla que llevara el registro
                controladorCorreoEnviado.InsertarCorreoEnviado(correoEnviadoTemporal);

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EN EL ENVIO: " + ex.Message);
            }
        }

        public static string ObtenerFechaFormatoDDMMYYYY(string fecha)
        {
            List<string> elementosFecha = fecha.Split('-').ToList<string>();
            return elementosFecha[2] + "/" + elementosFecha[1] + "/" + elementosFecha[0];
        }

        public static void EnviarCorreoYGuardarRegistro(string destinatario, string asunto, string body, List<DatosAlertta> registrosDatosAlerttaObtenidos, ControladorCorreoEnviado controladorCorreoEnviado, CorreoEnviado correoEnviadoTemporal, List<string> datosFechaHoras)
        {

            string bodyConDatos;
            
            bodyConDatos = body + "<span style=\"font-size:12pt; font-weight:bold\">" +
                " FECHA ENVIO : " + ObtenerFechaFormatoDDMMYYYY(datosFechaHoras[0]) + " <br>" +
                " ENTRE : " + datosFechaHoras[1] + " HRS Y " + datosFechaHoras[2] + " HRS " +
            "</span>";

            bodyConDatos += "<br>";

            int contadorEntregaOK = 0;
            //int contadorEntregaAUN_NO_ENTREGA = 0;
            int contadorEntregaRECHAZADA = 0;

            foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                if (registro.Entregado == "ENTREGA OK")
                    ++contadorEntregaOK;
            /*
            foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                if (registro.Entregado == "AUN NO ENTREGA")
                    ++contadorEntregaAUN_NO_ENTREGA;
                    */
            foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                if (registro.Entregado == "ENTREGA RECHAZADA")
                    ++contadorEntregaRECHAZADA;

            bodyConDatos += "<span style=\"font-size:12pt; font-weight:bold\">" +
            " ENTREGAS SIN PROBLEMA :" + contadorEntregaOK + "&nbsp;&nbsp;<br>" +
            //" AUN NO SE ENTREGAN :" + contadorEntregaAUN_NO_ENTREGA + "&nbsp;&nbsp;<br>" +
            " ENTREGAS RECHAZADAS:" + contadorEntregaRECHAZADA + "&nbsp;&nbsp;<br>" +
            "</span>";

            //bodyConDatos += "<br>";
            /*
            bodyConDatos += "<br><br><span style=\"font-size:12pt; font-weight:bold\"> DETALLES DE LAS QUE AUN NO SE ENTREGAN </span><br>";
            bodyConDatos += "<table>";
            bodyConDatos += "<tr><td><b>Numero Entrega</b></td><td><b>Nombre</b></td><td><b>Placa</b></td><td><b>Productos</b></td></tr>";
            foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                if (registro.Entregado == "AUN NO ENTREGA")
                    bodyConDatos += "<tr><td>"+ registro.NroEntrega + "</td><td><b>" + registro.Nombre + "</b></td><td>" + registro.Placa + "</td><td><b>" + registro.NombreProducto + "</b></td></tr>";
            bodyConDatos += "</table>";
            */

            if (contadorEntregaRECHAZADA > 0)
            {
                bodyConDatos += "<br><span style=\"font-size:12pt; font-weight:bold\"> DETALLES DE ENTREGAS RECHAZADAS </span><br>";
                bodyConDatos += "<table style='font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;width: 100%;' border='1'>";

                bodyConDatos += "<tr style='background-color: #f2f2f2;' >";
                
                bodyConDatos += "<td><b>Nombre Linea</b></td>";
                bodyConDatos += "<td><b>Nombre Operador</b></td>";
                bodyConDatos += "<td><b>Placa</b></td>";
                bodyConDatos += "<td><b>Numero Entrega</b></td>";
                bodyConDatos += "<td><b>Nombre Cliente</b></td>";
                bodyConDatos += "<td><b>Productos</b></td>";
                bodyConDatos += "<td><b>Fecha y Hora Entrega</b></td>";
                bodyConDatos += "<td><b>Motivo Rechazo</b></td>";

                bodyConDatos += "</tr>";

                foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                    if (registro.Entregado == "ENTREGA RECHAZADA")
                    {
                        bodyConDatos += "<tr>";
                        bodyConDatos += "<td valign='top' >" + registro.Linea + "</td>";
                        bodyConDatos += "<td valign='top' ><b>" + registro.Nombre + "</b></td>";
                        bodyConDatos += "<td valign='top' >" + registro.Placa + "</td>";
                        bodyConDatos += "<td valign='top' >" + registro.NroEntrega + "</td>";
                        bodyConDatos += "<td valign='top' >" + registro.NombreCliente + "</td>";
                        bodyConDatos += "<td valign='top' ><b>" + registro.NombreProducto + "<br></b></td>";
                        bodyConDatos += "<td valign='top' >" + registro.FechaEntrega + "<br></td>";
                        bodyConDatos += "<td valign='top' ><b>" + registro.MotivoRechazo + "<br></b></td>";
                        
                        bodyConDatos += "</tr>";
                    }

                bodyConDatos += "</table>";
            }

            if (contadorEntregaOK > 0)
            {
                bodyConDatos += "<br><span style=\"font-size:12pt; font-weight:bold\"> DETALLES DE ENTREGAS SIN PROBLEMA </span><br>";
                bodyConDatos += "<table  style='font-family: Arial, Helvetica, sans-serif;border-collapse: collapse;width: 100%;'  border='1'>";
                
                bodyConDatos += "<tr style='background-color: #f2f2f2;'>";

                bodyConDatos += "<td><b>Nombre Linea</b></td>";
                bodyConDatos += "<td><b>Nombre Operador</b></td>";
                bodyConDatos += "<td><b>Placa</b></td>";
                bodyConDatos += "<td><b>Numero Entrega</b></td>";
                bodyConDatos += "<td><b>Nombre Cliente</b></td>";
                bodyConDatos += "<td><b>Productos</b></td>";
                bodyConDatos += "<td><b>Fecha y Hora Entrega</b></td>";


                bodyConDatos += "</tr>";

                foreach (DatosAlertta registro in registrosDatosAlerttaObtenidos)
                    if (registro.Entregado == "ENTREGA OK")
                    {
                        
                        bodyConDatos += "<tr>";
                        bodyConDatos += "<td valign='top' >" + registro.Linea + "</td>";
                        bodyConDatos += "<td valign='top' ><b>" + registro.Nombre + "</b></td>";
                        bodyConDatos += "<td valign='top' >" + registro.Placa + "</td>";
                        bodyConDatos += "<td valign='top' >" + registro.NroEntrega + "</td>";
                        bodyConDatos += "<td valign='top' >" + registro.NombreCliente + "</td>";
                        bodyConDatos += "<td valign='top' ><b>" + registro.NombreProducto + "<br></b></td>";
                        bodyConDatos += "<td valign='top' >" + registro.FechaEntrega + "<br></td>";
                        bodyConDatos += "</tr>";
                    }

                bodyConDatos += "</table>";
            }

            try
            {
                Console.WriteLine("enviando...");

                if (VerificarExistenciaEntregas(contadorEntregaRECHAZADA, contadorEntregaOK))
                {
                    EnviarCorreo(destinatario, asunto, bodyConDatos);
                    Console.WriteLine("correo enviado");
                }
                else {
                    Console.WriteLine("no se envio correo por que no hay entregas que notificar");
                }
                
                //guardar listado de envio de correo en tabla que llevara el registro
                //controladorCorreoEnviado.InsertarCorreoEnviado(correoEnviadoTemporal);

            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR EN EL ENVIO: " + ex.Message);
            }
        }

        public static bool VerificarExistenciaEntregas(int contadorEntregaRECHAZADA,int contadorEntregaOK) {
            return ((contadorEntregaRECHAZADA + contadorEntregaOK) > 0);
        }

        public static void IterarDestinatarios(string[] destinatarios, EntregaMovilPrueba registro, string asunto, string body, ControladorCorreoEnviado controladorCorreoEnviado)
        {
            foreach (string destinatario in destinatarios)
            {
                CorreoEnviado correoEnviadoTemporal = new CorreoEnviado();

                correoEnviadoTemporal.mob_nro_ent = registro.mob_nro_ent;
                correoEnviadoTemporal.correo_al_que_se_envio = destinatario;
                correoEnviadoTemporal.fecha_en_la_que_se_envio = DateTime.UtcNow.Date;

                Boolean yaFueEnviadoUnCorreo = controladorCorreoEnviado.YaFueEnviadoUnCorreo(correoEnviadoTemporal);

                if (!yaFueEnviadoUnCorreo)
                    EnviarCorreoYGuardarRegistro(destinatario, asunto, body, registro, controladorCorreoEnviado, correoEnviadoTemporal);
                else
                    Console.WriteLine("No se envio por que ya fue enviado un correo con estos datos al destinatario");

            }
        }

        public static void IterarDestinatarios(string[] destinatarios, List<DatosAlertta> registrosDatosAlerttaObtenidos, string asunto, string body, ControladorCorreoEnviado controladorCorreoEnviado, List<string> datosFechaHoras)
        {
            foreach (string destinatario in destinatarios)
            {

                CorreoEnviado correoEnviadoTemporal = new CorreoEnviado();

                correoEnviadoTemporal.mob_nro_ent = 1;//registro.mob_nro_ent;
                correoEnviadoTemporal.correo_al_que_se_envio = destinatario;
                correoEnviadoTemporal.fecha_en_la_que_se_envio = DateTime.UtcNow.Date;

                Boolean yaFueEnviadoUnCorreo = controladorCorreoEnviado.YaFueEnviadoUnCorreo(correoEnviadoTemporal);

                if (!yaFueEnviadoUnCorreo)
                    EnviarCorreoYGuardarRegistro(destinatario, asunto, body, registrosDatosAlerttaObtenidos, controladorCorreoEnviado, correoEnviadoTemporal, datosFechaHoras);
                else
                    Console.WriteLine("No se envio por que ya fue enviado un correo con estos datos al destinatario");

            }
        }

        public static void IterarRegistros(List<EntregaMovilPrueba> registrosPruebaObtenidos, string asunto, string body, string mailsDestino)
        {
            string[] destinatarios = mailsDestino.Split(';');

            foreach (EntregaMovilPrueba registro in registrosPruebaObtenidos)
                IterarDestinatarios(destinatarios, registro, asunto, body, new ControladorCorreoEnviado());
        }

        public static void IterarRegistros(List<DatosAlertta> registrosDatosAlerttaObtenidos, string asunto, string body, string mailsDestino, List<string> datosFechaHoras)
        {
            string[] destinatarios = mailsDestino.Split(';');

            IterarDestinatarios(destinatarios, registrosDatosAlerttaObtenidos, asunto, body, new ControladorCorreoEnviado(), datosFechaHoras);
        }

        public static List<string> obtenerDatosFechaHoras()
        {
            List<string> datosFechaHoras = new List<string>();

            DateTime fechaHoy = DateTime.Now;

            int anioActual = fechaHoy.Year;
            int mesActual = fechaHoy.Month;
            int diaActual = fechaHoy.Day;

            //0-23
            /*
            int horaActual = fechaHoy.Hour;
            int horaAnterior = horaActual - 1;
            */

            int numeroHorasAdelantoServidor = 5;

            int horaActual = fechaHoy.Hour- numeroHorasAdelantoServidor;
            int horaAnterior = horaActual - 1;

            int minutoActual = fechaHoy.Minute;
            string cadenaMinutoActual=(minutoActual < 10) ? "0" + minutoActual : minutoActual.ToString();

            //int horaActual = 10;//fechaHoy.Hour;//
            //int horaAnterior = 9;//horaActual - 5;
            /*
            string cadenaHoraActual = ((horaActual < 10) ? "0" + horaActual : horaActual.ToString()) + ":00";
            string cadenaHoraAnterior = ((horaAnterior < 10) ? "0" + horaAnterior : horaAnterior.ToString()) + ":00";
            */

            string cadenaHoraActual = ((horaActual < 10) ? "0" + horaActual : horaActual.ToString()) + ":"+ cadenaMinutoActual;
            string cadenaHoraAnterior = ((horaAnterior < 10) ? "0" + horaAnterior : horaAnterior.ToString()) + ":"+ cadenaMinutoActual;

            string cadenaMesActual = (mesActual < 10) ? "0" + mesActual : mesActual.ToString();
            string cadenaDiaActual = (diaActual < 10) ? "0" + diaActual : diaActual.ToString();

            string fechaActual = anioActual + "-" + cadenaMesActual + "-" + cadenaDiaActual;

            datosFechaHoras.Add(fechaActual);
            datosFechaHoras.Add(cadenaHoraAnterior);
            datosFechaHoras.Add(cadenaHoraActual);

            return datosFechaHoras;
        }

        public static void Main(string[] args)
        {
            //int numeroIteraciones = 0;
            //SEGUNDOS_ESPERA = Convert.ToInt32(ConfigurationManager.AppSettings["SEGUNDOS_ESPERA"].ToString());

            Console.WriteLine("Ejecutando consola de alertas de entrega ecommerce...");

            //var tiempoInicio = TimeSpan.Zero;
            ////obtener tiempo espera de variable de config
            //var periodoTiempoEspera = TimeSpan.FromSeconds(SEGUNDOS_ESPERA);

            //var temporizador = new System.Threading.Timer(
                //(evento) =>
                //{
                    //numeroIteraciones += 1;
                    //Console.WriteLine("Numero Iteraciones: " + numeroIteraciones);

                    List<string> datosFechaHoras = obtenerDatosFechaHoras();

                    List<DatosAlertta> registrosDatosAlertta = new ControladorEntregaMovilPrueba().ObtenerTodosRegistrosAlerta(datosFechaHoras);
                    IterarRegistros(registrosDatosAlertta, ObtenerAsunto(), ObtenerCuerpoCorreo(), ObtenerMailsDestino(), datosFechaHoras);

            //}, null, tiempoInicio, periodoTiempoEspera);

            //Console.ReadLine();

            Console.WriteLine("Terminando consola de alertas de entrega ecommerce...");
        }

    }
}

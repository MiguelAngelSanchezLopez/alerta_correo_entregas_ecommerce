﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Controladores
{
    public class ControladorGeneral
    {
        public SqlConnection conexionSQL;
        public SqlCommand comandoSQL;
        public SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = ConfigurationManager.AppSettings["CADENA_CONEXION"].ToString();
        
        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public string EjecutarProcedimientoAlmacenadoEscalar()
        {
            return comandoSQL.ExecuteScalar().ToString();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
    }
}

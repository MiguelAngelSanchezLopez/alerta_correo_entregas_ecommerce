﻿using CapaDatos.Controladores;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaEnvioCorreo.Controladores
{
    public class ControladorRevisionUltimaInformacionLogisticaInversa : ControladorGeneral
    {
        public string cadenaFechaUltimaCreacionObtenida = "";

        public Boolean NoHayNotificacionViajesLogisticaInversa()
        {
            try
            {
                int numeroHorasLimiteEspera = Convert.ToInt32(ConfigurationManager.AppSettings["HORAS_ESPERA"].ToString());

                InicializarConexion();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("OBTENER_FECHA_ULTIMA_CREACION_INFORMACION_LOGISTICA_INVERSA");

                cadenaFechaUltimaCreacionObtenida = EjecutarProcedimientoAlmacenadoEscalar().ToString();

                conexionSQL.Close();

                DateTime fechaUltimaCreacionObtenida = DateTime.Parse(cadenaFechaUltimaCreacionObtenida);

                DateTime fechaActual = DateTime.Now;

                int anioActual = fechaActual.Year;
                int mesActual = fechaActual.Month;
                int diaActual = fechaActual.Day;
                int horaActual = fechaActual.Hour;
                int minutoActual = fechaActual.Minute;
                //int segundoActual = fechaActual.Second;

                int anioUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Year;
                int mesUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Month;
                int diaUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Day;
                int horaUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Hour;
                int minutoUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Minute;
                //int segundoUltimaCreacionObtenida = fechaUltimaCreacionObtenida.Second;

                if (anioActual > anioUltimaCreacionObtenida)
                    return true;

                if (mesActual > mesUltimaCreacionObtenida)
                    return true;

                if (diaActual > diaUltimaCreacionObtenida)
                    return true;

                if (horaActual >= (horaUltimaCreacionObtenida + numeroHorasLimiteEspera))
                    return true;
                //if (horaActual >= (horaUltimaCreacionObtenida+2))
                //if (minutoActual >= (minutoUltimaCreacionObtenida+1))
                //    return true;

                Console.WriteLine("no se envio el correo por que aun no se cumple el tiempo limite");

                return false;

            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
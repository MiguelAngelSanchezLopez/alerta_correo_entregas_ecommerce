﻿using CapaDatos.Modelos;
using PruebaEnvioCorreo.Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Controladores
{
    public class ControladorEntregaMovilPrueba:ControladorGeneral
    {
        private List<EntregaMovilPrueba> listadoRegistrosPrueba;
        private List<DatosAlertta> listadoRegistrosDatosAlertta;

        public EntregaMovilPrueba LlenarRegistroPrueba()
        {
            EntregaMovilPrueba RegistroPruebaTemporal = new EntregaMovilPrueba();
            RegistroPruebaTemporal.mob_nro_ent = Convert.ToInt32(lectorDatosSQL["mob_nro_ent"]);
            RegistroPruebaTemporal.mob_cab_nro_int = Convert.ToInt32(lectorDatosSQL["mob_cab_nro_int"]);
            RegistroPruebaTemporal.mob_cab_id_usr = Convert.ToInt32(lectorDatosSQL["mob_cab_id_usr"]);
            return RegistroPruebaTemporal;
        }

        public DatosAlertta LlenarRegistroDatosAlertta()
        {
            
            DatosAlertta RegistroDatosAlerttaTemporal = new DatosAlertta();

            RegistroDatosAlerttaTemporal.FechaSubidaViaje = lectorDatosSQL["FechaSubidaViaje"].ToString();
            RegistroDatosAlerttaTemporal.NroEntrega = lectorDatosSQL["NroEntrega"].ToString();
            RegistroDatosAlerttaTemporal.Nombre = lectorDatosSQL["Nombre"].ToString();
            RegistroDatosAlerttaTemporal.Entregado = lectorDatosSQL["Entregado"].ToString();
            RegistroDatosAlerttaTemporal.FechaEntrega = lectorDatosSQL["FechaEntrega"].ToString();
            RegistroDatosAlerttaTemporal.MotivoRechazo = lectorDatosSQL["MotivoRechazo"].ToString();
            RegistroDatosAlerttaTemporal.Placa = lectorDatosSQL["Placa"].ToString();
            RegistroDatosAlerttaTemporal.Cant_dias = lectorDatosSQL["Cant_dias"].ToString();
            RegistroDatosAlerttaTemporal.LatProgramado = lectorDatosSQL["LatProgramado"].ToString();
            RegistroDatosAlerttaTemporal.LonProgramado = lectorDatosSQL["LonProgramado"].ToString();
            RegistroDatosAlerttaTemporal.LatEntrega = lectorDatosSQL["LatEntrega"].ToString();
            RegistroDatosAlerttaTemporal.LonEntrega = lectorDatosSQL["LonEntrega"].ToString();
            RegistroDatosAlerttaTemporal.MotivoSeleccionado = lectorDatosSQL["MotivoSeleccionado"].ToString();
            RegistroDatosAlerttaTemporal.TotalViajes = lectorDatosSQL["TotalViajes"].ToString();
            RegistroDatosAlerttaTemporal.NombreProducto = lectorDatosSQL["NombreProducto"].ToString();

            //RegistroDatosAlerttaTemporal.Linea = "linea transporte";//lectorDatosSQL["Linea"].ToString();
            RegistroDatosAlerttaTemporal.Linea = lectorDatosSQL["Linea"].ToString();
            RegistroDatosAlerttaTemporal.NombreCliente = lectorDatosSQL["NombreCliente"].ToString();

            return RegistroDatosAlerttaTemporal;
            
        }

        public void LlenarListadoRegistrosPrueba()
        {
            while (lectorDatosSQL.Read())
                listadoRegistrosPrueba.Add(LlenarRegistroPrueba());
        }
        
        public void LlenarListadoRegistrosDatosAlertta()
        {
            while (lectorDatosSQL.Read())
                listadoRegistrosDatosAlertta.Add(LlenarRegistroDatosAlertta());
        }

        public List<EntregaMovilPrueba> ObtenerTodosRegistrosPrueba()
        {
            try
            {
                InicializarConexion();

                listadoRegistrosPrueba = new List<EntregaMovilPrueba>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("OBTENER_REGISTROS_PRUEBAS");

                LlenarLectorDatosSQL();

                LlenarListadoRegistrosPrueba();

                conexionSQL.Close();

                return listadoRegistrosPrueba;
            }
            catch (Exception ex)
            {
                return new List<EntregaMovilPrueba>();
            }

        }
        
        public List<DatosAlertta> ObtenerTodosRegistrosAlerta(List<string> datosFechaHoras)
        {
            try
            {
                InicializarConexion();

                listadoRegistrosDatosAlertta = new List<DatosAlertta>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("OBTENER_DATOS_ENTREGAS_PARA_ALERTA");
                
                EstablecerParametroProcedimientoAlmacenado(datosFechaHoras[0], "string", "@FECHA_BUSQUEDA");
                EstablecerParametroProcedimientoAlmacenado(datosFechaHoras[1], "string", "@HORA_INICIO_BUSQUEDA");
                EstablecerParametroProcedimientoAlmacenado(datosFechaHoras[2], "string", "@HORA_FIN_BUSQUEDA");

                LlenarLectorDatosSQL();

                LlenarListadoRegistrosDatosAlertta();

                conexionSQL.Close();

                return listadoRegistrosDatosAlertta;
            }
            catch (Exception ex)
            {
                return new List<DatosAlertta>();
            }

        }
    }
}

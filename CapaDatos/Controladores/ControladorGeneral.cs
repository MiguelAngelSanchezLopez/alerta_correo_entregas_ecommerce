﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos.Controladores
{
    public class ControladorGeneral
    {
        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = "Data Source=test-altotrack-sql.clhc4wjn055a.us-east-1.rds.amazonaws.com;Initial Catalog=TRANSPORTE_ECOMMERCE_MX;User ID=sa_sqlaltotrack;Password=sa_sqlaltotrack2018";
        // "Data Source=altotrack-sql.clhc4wjn055a.us-east-1.rds.amazonaws.com;Initial Catalog=TRANSPORTE_WALMART_MX;User ID=transportewalmartmx;Password=#WalmartMexico2016#";

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public string EjecutarProcedimientoAlmacenadoEscalar()
        {
            return comandoSQL.ExecuteScalar().ToString();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
    }
}
